# HiggsSignals Dataset

This repository contains the dataset of measurements implemented in the HiggsSignals module of [HiggsTools]. 

[HiggsTools]: https://gitlab.com/higgsbounds/higgstools
